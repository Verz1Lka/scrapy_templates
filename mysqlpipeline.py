# -*- coding: utf-8 -*-
import MySQLdb
from scrapy.conf import settings
import logging
# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

# section for settings.py
# DB_HOST = 'localhost'
# DB_BASE = 'database'
# DB_USER = 'user'
# DB_PASSWORD = 'password'
# DB_PORT = 3306
# DB_TABLE = 'table'
# UPDATE_FOR_DUPLICATE = True

# ITEM_PIPELINES = {
#    'project.mysqlpipeline.MySQLPipeline': 300,
# }


class MySQLPipeline(object):
    conn = MySQLdb.connect(host=settings.get('DB_HOST'), user=settings.get('DB_USER'), port=settings.get('DB_PORT'),
                           passwd=settings.get('DB_PASSWORD'), db=settings.get('DB_BASE'), charset='utf8')
    cur = conn.cursor()
    cur.execute('SET names UTF8')
    table = settings.get('DB_TABLE')
    update_for_duplicate = settings.get('UPDATE_FOR_DUPLICATE')

    def process_item(self, item, spider):
        keys = u'`, `'.join(item.keys())
        values = u"','".join([unicode(val).replace("'", "''").replace('\\', '\\\\') for val in item.values()])
        if self.update_for_duplicate:
            update = ', '.join([u"`{}` = '{}'".format(
                key, unicode(item[key]).replace("'", "''").replace('\\', '\\\\')) for key in item])
            query = u"INSERT INTO {} (`{}`) VALUES ('{}') ON DUPLICATE KEY UPDATE {}".format(
                self.table, keys, values, update)
        else:
            query = u"INSERT INTO {} (`{}`) VALUES ('{}')".format(self.table, keys, values)
        try:
            self.cur.execute(query)
            self.conn.commit()
        except MySQLdb.Error as e:
            logging.info(e)
            logging.info(u'BAD QUERY: {}'.format(query))
        return item
